const _ = require('lodash');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const audience = 'AuthTestApp';

exports.genToken = (user, pass) => {
  return jwt.sign({ user, pass }, audience);
};

exports.verifyToken = (token) => {
  console.log('Verifying token...');
  return jwt.verify(token, audience, (err, decoded) => {
    if (err) throw new Error('Invalid Token: ' + err);
    console.log(decoded);
  });
};

exports.authMiddleware = async (req, res, next) => {
  const authHeader = req.header('Authorization');
  console.log('=======================================');
  console.log("Getting token from request's header...");
  if (!authHeader) return res.status(401).send('Not authorized');

  const token = _.replace(authHeader, 'Bearer', '').trim();
  if (!token) return res.status(401).send('Not authorized');

  try {
    console.log('Decoding...');
    const decode = jwt.verify(token, audience, (err, decoded) => {
      if (err) throw new Error('Invalid Token: ' + err);
      console.log(decoded);
    });
    req.user = decode;
    next();
  } catch (error) {
    return res.status(401).send('Not authorized');
  }
};
