# NodeJS | **Autenticación basada en tokens**

Repositorio que contiene un PoC de autenticación basada en tokens para un API en NodeJS.

Contiene:

- Firmado y validación de tokens con HMAC-SHA256
- Middleware para protección de APIS Rest con tokens JWT.
- Ejemplo de implementación en API REST.
