const express = require('express');
const auth = require('./middlewares/auth');
const bodyParser = require('body-parser');

const app = express();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/login', (req, res) => {
  const { user, pass } = req.body;
  console.log('User:', user);
  console.log('Pass:', pass);
  const token = auth.genToken(user, pass);
  console.log('Token generated!');
  console.log('=======================================');

  return res.json({ token });
});

app.get('/languages', auth.authMiddleware, (req, res) => {
  return res.json({
    data: [
      { name: 'JavaScript' },
      { name: 'C#' },
      { name: 'Python' },
      { name: 'SQL' },
      { name: 'Ruby' },
      { name: 'Java' },
    ],
  });
});

app.listen(port, () => {
  console.log('=======================================');
  console.log('NodeJS API | Token-Based Authentication');
  console.log('=======================================');
  console.log(`Server running at http//localhost:${port}/`);
  console.log('=======================================');
});
